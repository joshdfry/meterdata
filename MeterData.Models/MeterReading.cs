﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MeterData.Models
{
    [Table("MeterReadings")]
    public class MeterReading
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime MeterReadingDateTime { get; set; }
        public int MeterReadValue { get; set; }
        [ForeignKey(nameof(Account))]
        public int AccountId { get; set; }
        #nullable enable
        public Account? Account { get; set; }
    }
}
