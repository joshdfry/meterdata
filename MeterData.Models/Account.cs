﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeterData.Models
{
    [Table("Accounts")]
    public class Account
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
#nullable enable
        public ICollection<MeterReading>? MeterReadings { get; set; }
    }
}
