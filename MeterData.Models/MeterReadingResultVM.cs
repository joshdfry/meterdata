﻿namespace MeterData.Models
{
    public class MeterReadingResultVM
    {
        public int InvalidRowCount { get; set; }
        public int ValidRowCount { get; set; }
    }
}
