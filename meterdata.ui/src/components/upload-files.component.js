import React, { Component } from "react";
import UploadService from "../services/upload-files.service";
export default class UploadFiles extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFiles: [],
            currentFile: [],
            progress: 0,
            message: "",
            fileInfos: [],
        };
    }
    selectFile(event) {
        this.setState({
            selectedFiles: event.target.files,
            progress: 0,
            message: ""
        });
    }
    upload() {
        let currentFile = this.state.selectedFiles[0];
        this.setState({
            progress: 0,
            currentFile: currentFile,
        });
        UploadService.upload(currentFile, (event) => {
            this.setState({
                progress: Math.round((100 * event.loaded) / event.total),
            });
        })
            .then((response) => {
                console.log(response);
                this.setState({
                    message: "Uploaded Successfully! - " + response.data.validRowCount + " valid rows, " + response.data.invalidRowCount + " invalid rows"
                });
            })
            .catch(() => {
                this.setState({
                    progress: 0,
                    message: "Could not upload the file!",
                    currentFile: undefined,
                });
            });
        this.setState({
            selectedFiles: undefined,
        });
    }
    componentDidMount() {
    }
    render() {
        const {
            selectedFiles,
            currentFile,
            progress,
            message,
            fileInfos,
        } = this.state;
        return (
            <div>
                {currentFile && (
                    <div className="progress">
                        <div
                            className="progress-bar progress-bar-info progress-bar-striped"
                            role="progressbar"
                            aria-valuenow={progress}
                            aria-valuemin="0"
                            aria-valuemax="100"
                            style={{ width: progress + "%" }}
                        >
                            {progress}%
                        </div>
                    </div>
                )}
                <label className="btn btn-default">
                    <input type="file" onChange={this.selectFile.bind(this)} />
                </label>
                <button className="btn btn-success"
                    disabled={!selectedFiles}
                    onClick={this.upload.bind(this)}
                >
                    Upload
                </button>
                <div className="alert alert-light" role="alert">
                    {message}
                </div>
            </div>
        );
    }
}