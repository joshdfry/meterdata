using MeterData.Contracts;
using MeterData.Contracts.ServiceInterfaces;
using MeterData.Services;
using NUnit.Framework;
using Moq;
using System.Linq;
using System.Collections.Generic;
using MeterData.Models;
using System.IO;
using System;

namespace MeterData.Tests
{
    [TestFixture]
    public class Tests
    {
        private Mock<IRepositoryWrapper> _mockRepositoryWrapper;
        private IRepositoryWrapper _repositoryWrapper;
        private ICSVUtilities _csvUtils;
        private IMeterReadingService _meterReadingService;
        private string _csvString;
        [SetUp]
        public void SetUp()
        {
            _mockRepositoryWrapper = new Mock<IRepositoryWrapper>();
            _mockRepositoryWrapper.Setup(x => x.Account.FindAll()).Returns(GetAccounts());
            _mockRepositoryWrapper.Setup(x => x.MeterReading.Create(new MeterReading()));
            _repositoryWrapper = _mockRepositoryWrapper.Object;
            _csvUtils = new CSVUtilities();
            _meterReadingService = new MeterReadingService(_repositoryWrapper);
            StreamReader sr = new StreamReader("TestingFiles/Meter_Reading.csv");
            _csvString = sr.ReadToEnd();
        }

        [Test]
        public void ParseString_Should_return_35_rows()
        {
            var result = _csvUtils.ParseStringToRowsAndCells(_csvString);
            Assert.AreEqual(result.Count(), 35);
        }

        [Test]
        public void MeterReadingServce_CheckValidationRules()
        {
            // Check that rows with incorrect Number of cells cannot be added
            var wrongNumberOfCells = new List<string[]>() { new string[] { "2344", "22/04/2019 09:24" }, new string[] { "2233", "22/04/2019 09:24", "01002", "EXTRACELL" } };
            var wrongNumberOfCellsResult = _meterReadingService.ReturnValidRows(wrongNumberOfCells);
            Assert.AreEqual(wrongNumberOfCellsResult.Count(), 0);

            // Check that duplicate rows cannot be added
            var duplicateRows = new List<string[]>() { new string[] { "2344", "22/04/2019 09:24", "01002" }, new string[] { "2344", "22/04/2019 09:24", "01002" } };
            var duplicateResult = _meterReadingService.ReturnValidRows(duplicateRows);
            Assert.AreEqual(duplicateResult.Count(), 1);

            // Check that rows with non-existant account cannot be added
            var nonExistantAccount = new List<string[]>() { new string[] { "2999", "22/04/2019 09:24", "01002" }};
            var nonExistantAccountResult = _meterReadingService.ReturnValidRows(nonExistantAccount);
            Assert.AreEqual(nonExistantAccountResult.Count(), 0);

            // Check that invalid meter reading cannot be added
            var meterReadingRows = new List<string[]>() { new string[] { "2344", "22/04/2019 09:24", "1002" }, new string[] { "2233", "22/04/2019 09:24", "-3002" }, new string[] { "2345", "22/04/2019 09:24", "01002" } };
            var meterReadingResult = _meterReadingService.ReturnValidRows(meterReadingRows);
            Assert.AreEqual(meterReadingResult.Count(), 1);

            // Check Total Number of valid rows using test data
            var totalRowsResult = _meterReadingService.ReturnValidRows(_csvUtils.ParseStringToRowsAndCells(_csvString));
            Assert.AreEqual(totalRowsResult.Count(), 24);
        }

        [Test]
        public void MeterReadingServce_CheckValidDataAddToRepo()
        {
            // Not good testing practice to rely on other functionality for testing data, but to save time for this challenge:
            var validRows = _meterReadingService.ReturnValidRows(_csvUtils.ParseStringToRowsAndCells(_csvString));
            //
            _meterReadingService.AddRowsToRepo(validRows);
        }
        
        [Test]
        public void MeterReadingServce_InvalidDataAddToRepo()
        {
            var invalidDate = new List<string[]>() { new string[] { "2344", "august 3rd", "1002" } }; // invalid date
            Assert.Throws<FormatException>(() => _meterReadingService.AddRowsToRepo(invalidDate));
        }

        private IQueryable<Account> GetAccounts() 
        { 
            var accounts = new List<Account>();
            accounts.Add(new Account() { AccountId = 2344, FirstName = "Tommy", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2233, FirstName = "Barry", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 8766, FirstName = "Sally", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2345, FirstName = "Jerry", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2346, FirstName = "Ollie", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2347, FirstName = "Tara", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2348, FirstName = "Tammy", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2349, FirstName = "Simon", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2350, FirstName = "Colin", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2351, FirstName = "Gladys", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2352, FirstName = "Greg", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2353, FirstName = "Tony", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2355, FirstName = "Arthur", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 2356, FirstName = "Craig", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 6776, FirstName = "Laura", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 4534, FirstName = "JOSH", LastName = "TEST"});
            accounts.Add(new Account() { AccountId = 1234, FirstName = "Freya", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 1239, FirstName = "Noddy", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 1240, FirstName = "Archie", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 1241, FirstName = "Lara", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 1242, FirstName = "Tim", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 1243, FirstName = "Graham", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 1244, FirstName = "Tony", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 1245, FirstName = "Neville", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 1246, FirstName = "Jo", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 1247, FirstName = "Jim", LastName = "Test"});
            accounts.Add(new Account() { AccountId = 1248, FirstName = "Pam", LastName = "Test"});
            return accounts.AsQueryable();
        }
    }
}