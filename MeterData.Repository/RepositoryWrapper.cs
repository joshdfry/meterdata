﻿using MeterData.Contracts;

namespace MeterData.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repoContext;
        private IMeterReadingRepository _meterReading;
        private IAccountRepository _account;
        public IMeterReadingRepository MeterReading
        {
            get
            {
                if (_meterReading == null)
                {
                    _meterReading = new MeterReadingRepository(_repoContext);
                }
                return _meterReading;
            }
        }
        public IAccountRepository Account
        {
            get
            {
                if (_account == null)
                {
                    _account = new AccountRepository(_repoContext);
                }
                return _account;
            }
        }
        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        public void Save()
        {
            _repoContext.SaveChanges();
        }
    }
}
