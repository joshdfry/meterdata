﻿using MeterData.Models;
using Microsoft.EntityFrameworkCore;

namespace MeterData.Repository
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options)
            : base(options)
        {
        }
#nullable enable
        public DbSet<Account>? Accounts { get; set; }
        public DbSet<MeterReading>? MeterReadings { get; set; }
    }
}
