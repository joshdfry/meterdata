﻿using MeterData.Contracts;
using MeterData.Models;

namespace MeterData.Repository
{
    public class MeterReadingRepository : RepositoryBase<MeterReading>, IMeterReadingRepository
    {
        public MeterReadingRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
