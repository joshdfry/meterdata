﻿using MeterData.Contracts.ServiceInterfaces;
using System.Collections.Generic;
using System.Linq;

namespace MeterData.Services
{
    public class CSVUtilities : ICSVUtilities
    {
        public List<string[]> ParseStringToRowsAndCells(string csvString)
        {
            var RowsAndCells = new List<string[]>();
            var isHeader = true;
            csvString.Split('\n').ToList().ForEach(row =>
            {
                if (!isHeader)
                {
                    RowsAndCells.Add(row.Split(',').ToList().ToArray());
                }
                isHeader = false;
            });
            return RowsAndCells;
        }
    }
}
