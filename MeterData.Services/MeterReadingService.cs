﻿using MeterData.Contracts;
using MeterData.Contracts.ServiceInterfaces;
using MeterData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace MeterData.Services
{
    public class MeterReadingService : IMeterReadingService
    {
        private readonly IRepositoryWrapper _repository;
        public MeterReadingService(IRepositoryWrapper repository)
        {
            _repository = repository;
        }
        public void AddRowsToRepo(List<string[]> inputRows)
        {
            var meterReadings = new List<MeterReading>();
            inputRows.ForEach(x => 
            {
                var meterReading = new MeterReading();
                meterReading.MeterReadingDateTime = DateTime.Parse(x[1]);
                meterReading.MeterReadValue = int.Parse(x[2]);
                meterReading.AccountId = int.Parse(x[0]);
                _repository.MeterReading.Create(meterReading);
            });
            _repository.Save();
        }

        public List<string[]> ReturnValidRows(List<string[]> inputRows)
        {
            var existingAccounts = _repository.Account.FindAll();
            var verifiedList = new List<string[]>();
            foreach (var row in inputRows)
            {
                // check for exactly 3 cells
                if (row.Length != 3)
                {
                    continue;
                }

                // check for identical entry
                if (verifiedList.FirstOrDefault(vl => (vl[0] == row[0] && vl[1] == row[1] && vl[2] == row[2])) != null)
                {
                    continue;
                }

                // Check if account id parses to int
                int t;
                if (!(Int32.TryParse(row[0], out t)))
                {
                    continue;
                }

                // Check for a matching account
                if (existingAccounts.FirstOrDefault(a => a.AccountId == t) == null)
                {
                    continue;
                }

                // Check for valid Reading format
                if (row[2].Length != 5)
                {
                    continue;
                }
                if (!Regex.IsMatch(row[2], @"^\d+$"))
                {
                    continue;
                }
                verifiedList.Add(row);

            }
            return verifiedList;
        }
    }
}
