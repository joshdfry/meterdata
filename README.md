# README #

### What is this repository for? ###

This repository provides an API endpoint for uploading a CSV meter reading spreadsheet and a small front end client to consume the endpoint.
Unit tests are also included.

### How do I get set up? ###
The backend API is currently setup with an MSSQL DB using Entity Framework for the data repo. You will need to run the migrations to build the tables:
dotnet ef database update -s MeterData.API
 
The Unit Tests should work straight away as the DB is mocked.

Front end React client will require an npm install to install the packaged and then npm start to run the client.

### Who do I talk to? ###
Josh Fry - 0404 200 512 - joshdfry@gmail.com