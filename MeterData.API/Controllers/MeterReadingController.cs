﻿using MeterData.Contracts;
using MeterData.Contracts.ServiceInterfaces;
using MeterData.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Linq;

namespace MeterData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MeterReadingController : ControllerBase
    {
        private IRepositoryWrapper _repository;
        private ICSVUtilities _csvUtils;
        private IMeterReadingService _meterReadingService;
        public MeterReadingController(IRepositoryWrapper repository, ICSVUtilities csvUtils, IMeterReadingService meterReadingService)
        {
            _repository = repository;
            _csvUtils = csvUtils;
            _meterReadingService = meterReadingService;
        }

        [HttpPost]
        [Route("/api/meter-reading-uploads")] 
        // CODE CHALLENGE NOTE:
        // using this url because it was a requirement for the challenge, but if doing a standard-ish RESTful API I'd be
        // keeping the url as api/MeterReading and including GET, POST, PUT, DELETE all on the same url. 
        public IActionResult SubmitMeterReading([FromForm] IFormFile file)
        {
            string csvString; 
            using (Stream stream = new MemoryStream())
            {
                file.CopyTo(stream);
                stream.Position = 0;
                StreamReader sr = new StreamReader(stream);                
                csvString = sr.ReadToEnd();
            }
            var rowsAndCells = _csvUtils.ParseStringToRowsAndCells(csvString);
            var validatedRowsAndCells = _meterReadingService.ReturnValidRows(rowsAndCells);
            _meterReadingService.AddRowsToRepo(validatedRowsAndCells);
            return Ok(new MeterReadingResultVM() { InvalidRowCount = rowsAndCells.Count() - validatedRowsAndCells.Count(), ValidRowCount = validatedRowsAndCells.Count()});
        }
    }
}
