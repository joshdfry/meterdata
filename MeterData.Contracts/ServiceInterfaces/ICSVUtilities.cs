﻿using MeterData.Models;
using System.Collections.Generic;
using System.Data;

namespace MeterData.Contracts.ServiceInterfaces
{
    public interface ICSVUtilities
    {
        List<string[]> ParseStringToRowsAndCells(string csvString);
    }
}
