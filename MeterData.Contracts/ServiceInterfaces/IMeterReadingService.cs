﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeterData.Contracts.ServiceInterfaces
{
    public interface IMeterReadingService
    {
        List<string[]> ReturnValidRows(List<string[]> inputRows);
        void AddRowsToRepo(List<string[]> inputRows);
    }
}
