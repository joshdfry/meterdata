﻿namespace MeterData.Contracts
{
    public interface IRepositoryWrapper
    {
        IAccountRepository Account { get; }
        IMeterReadingRepository MeterReading { get; }
        void Save();
    }
}
