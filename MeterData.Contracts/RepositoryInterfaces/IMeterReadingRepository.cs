﻿using MeterData.Models;

namespace MeterData.Contracts
{
    public interface IMeterReadingRepository : IRepositoryBase<MeterReading>
    {
    }
}
