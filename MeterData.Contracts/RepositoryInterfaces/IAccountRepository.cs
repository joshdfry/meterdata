﻿using MeterData.Models;

namespace MeterData.Contracts
{
    public interface IAccountRepository : IRepositoryBase<Account>
    {
    }
}
